#include <iostream>

// including this header here generates an error:
//   error: unused variable ‘’ [-Werror=unused-variable]
// #include <lib.hpp>
#include "header_wrapper.h"

int main() {
    std::cout << "Hello, World!\n" 
              << warn2() << '\n';
}
