#ifdef __linux__
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"

#include <lib.hpp>

#pragma GCC diagnostic pop
#endif

#ifdef _WIN32
#pragma warning( push )
#pragma warning( disable : 4100 )

#include <lib.hpp>

#pragma warning( pop )
#endif
