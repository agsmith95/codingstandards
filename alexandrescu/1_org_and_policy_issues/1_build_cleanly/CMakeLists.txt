cmake_minimum_required(VERSION 3.10)
project(compile_cleanly)
set(CMAKE_CXX_STANDARD 17)

add_subdirectory(libs)

set(SOURCE_FILES main.cpp)
set(EXECUTABLE compile_cleanly)
add_executable(${EXECUTABLE} ${SOURCE_FILES})
target_link_libraries(
    ${EXECUTABLE}
    lib_warn
)

if(MSVC)
  target_compile_options(${EXECUTABLE} PRIVATE /W4 /WX)
else()
  target_compile_options(${EXECUTABLE} PRIVATE -Wall -Wextra -pedantic -Werror)
endif()
