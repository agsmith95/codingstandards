// Naming Conventions - be consistent!

#include <chrono> // not "#include < chrono >"
#include <string>

#define ALL_CAPS_ONLY_FOR_MACRO 42

using namespace std::string_literals;
using namespace std::chrono_literals;

int prefer_underscore_naming() 
{ // also, use "K & R"
    return ALL_CAPS_ONLY_FOR_MACRO;
}
// 

// When declaring a class use the following order
//    * types: classes, enums, and aliases (using)
//    * constructors, assignments, destructor
//    * functions
//    * data
// Use the public before protected before private order
template <typename T>
class Some_class {
public:
    // interface
    enum class Some_enum {
        Red,
        Green,
        Blue
    };

    Some_class();
    ~Some_class();

    int ret0() {return 0;};

    // Use C++-style declarator layout:
    T& operator[](size_t);   // OK
    // T &operator[](size_t);   // just strange
    // T & operator[](size_t);   // undecided
protected:
    // unchecked function for use by derived class implementations
private:
    // implementation details
    int* data;  // do this, not "int *data" or something else
                // how to be with multiple? - DON'T
                // Use one line per declaration!
};
//

// Avoid names that are easily misread:
int oO01lL = 6; // bad

int splunk = 7;
int splonk = 8; // bad: splunk and splonk are easily confused
//

// Don’t use void as an argument type
void f(void);   // bad
void g();       // better
//

// Use conventional const notation
const int x = 7;    // OK
int const y = 9;    // bad

const int *const p = nullptr;   // OK, constant pointer to constant int
// int const *const p = nullptr;// bad, constant pointer to constant int
//



int main(int argc, const char** argv) {
    // use readable literals
    // Example. Use digit separators to avoid long strings of digits:
    auto c = 299'792'458; // m/s2
    auto q2 = 0b0000'1111'0000'0000;
    auto ss_number = 123'456'7890;
    // Example. Use literal suffixes where clarification is needed:
    auto hello = "Hello!"s; // a std::string
    auto world = "world";   // a C-style string
    auto interval = 100ms;  // using <chrono>

    // Don’t place two statements on the same line
    int y = 7; const char* s = "Hello"; // don't
    int x = 7; x += 3; x = 29;          // don't
    //

    return 0;
}

