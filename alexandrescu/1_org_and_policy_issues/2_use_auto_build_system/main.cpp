#include <iostream>
#include "header1.h"

int main() {
    std::cout << "CMake handles incremental builds for you\n";
    std::cout << A{1,2,3,4,5}.sum() << '\n';
    A(28).print();
    return 0;
}
