#include <algorithm>
#include <numeric>
#include <iostream>
#include <iterator>
#include "header1.h"

A::A() {}

A::~A() {
    if (data != nullptr) {
        delete [] data;
    }
}

A::A(int size): data{new int[size]}, s{static_cast<size_t>(size)} {}
A::A(il<int> d): data{new int[d.size()]}, s{d.size()} {
    std::copy(d.begin(), d.end(), data);
}

int A::sum() {
    return std::accumulate(data, data+s, 0);
}

void A::print() {
    std::cout << '[';
    std::copy(data, data+s,
        std::ostream_iterator<int>(std::cout, " ")
    );
    std::cout << "]\n";
}