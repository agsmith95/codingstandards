#include <initializer_list>

template <typename T>
using il = std::initializer_list<T>;

class A {
public:
    A();
    ~A();

    A(int);
    A(il<int>);

    int sum();
    void print();

    A(const A&) = delete;
    A& operator=(const A&) = delete;
    A(A&&) = default;
    A& operator=(A&&) = default;
private:
    int* data = nullptr;
    std::size_t s;
};